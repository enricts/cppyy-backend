diff --git a/src/core/metacling/src/TClingMethodInfo.cxx b/src/core/metacling/src/TClingMethodInfo.cxx
index ca606db037..dcd03d38bd 100644
--- a/src/core/metacling/src/TClingMethodInfo.cxx
+++ b/src/core/metacling/src/TClingMethodInfo.cxx
@@ -82,6 +82,49 @@ private:
    Iterator fEnd;
 };
 
+class TClingMethodInfo::UsingIterator
+{
+public:
+   typedef clang::UsingDecl::shadow_iterator Iterator;
+
+   UsingIterator(cling::Interpreter* interp, Iterator begin, Iterator end) : fInterp(interp), fIter(begin), fEnd(end) {}
+   explicit UsingIterator(cling::Interpreter* interp, clang::UsingDecl *decl) :
+      fInterp(interp), fIter(decl->shadow_begin()), fEnd(decl->shadow_end()) {}
+
+   FunctionDecl *operator* () const {
+      clang::ConstructorUsingShadowDecl* shadow_ctor = llvm::dyn_cast<clang::ConstructorUsingShadowDecl>(*fIter);
+      if (shadow_ctor) {
+         clang::CXXConstructorDecl* base_ctor = llvm::dyn_cast<clang::CXXConstructorDecl>(shadow_ctor->getTargetDecl());
+         if (base_ctor) {
+            if (base_ctor->isImplicit()) return nullptr; // skip as Cling will generate these anyway
+            return fInterp->getSema().findInheritingConstructor(base_ctor->getLocStart(), base_ctor, shadow_ctor);
+         }
+      } else {
+         clang::UsingShadowDecl* shadow_decl = llvm::dyn_cast<clang::UsingShadowDecl>(*fIter);
+         if (shadow_decl) {
+            clang::CXXMethodDecl* method = llvm::dyn_cast<clang::CXXMethodDecl>(shadow_decl->getTargetDecl());
+            if (method) return method;
+         }
+      }
+      return llvm::dyn_cast<clang::FunctionDecl>((*fIter)->getTargetDecl());
+   }
+   FunctionDecl *operator-> () const { return this->operator*(); }
+   UsingIterator & operator++ () { ++fIter; return *this; }
+   UsingIterator   operator++ (int) {
+      UsingIterator tmp(fInterp, fIter,fEnd);
+      ++(*this);
+      return tmp;
+   }
+   bool operator!() { return fIter == fEnd; }
+   operator bool() { return fIter != fEnd; }
+
+private:
+   cling::Interpreter* fInterp;
+   Iterator fIter;
+   Iterator fEnd;
+};
+
+
 TClingMethodInfo::TClingMethodInfo(const TClingMethodInfo &rhs) :
    TClingDeclInfo(rhs),
    fInterp(rhs.fInterp),
@@ -90,13 +133,20 @@ TClingMethodInfo::TClingMethodInfo(const TClingMethodInfo &rhs) :
    fContextIdx(rhs.fContextIdx),
    fIter(rhs.fIter),
    fTitle(rhs.fTitle),
-   fTemplateSpecIter(nullptr)
+   fTemplateSpecIter(nullptr),
+   fUsingIter(nullptr)
 {
    if (rhs.fTemplateSpecIter) {
       // The SpecIterator query the decl.
       R__LOCKGUARD(gInterpreterMutex);
       fTemplateSpecIter = new SpecIterator(*rhs.fTemplateSpecIter);
    }
+
+   if (rhs.fUsingIter) {
+      // Internal loop over using shadow decls active
+      R__LOCKGUARD(gInterpreterMutex);
+      fUsingIter = new UsingIterator(*rhs.fUsingIter);
+   }
 }
 
 
@@ -112,6 +162,7 @@ TClingMethodInfo& TClingMethodInfo::operator=(const TClingMethodInfo &rhs) {
    fIter = rhs.fIter;
    fTitle = rhs.fTitle;
    fTemplateSpecIter = nullptr;
+   fUsingIter = nullptr;
 
    if (rhs.fTemplateSpecIter) {
       // The SpecIterator query the decl.
@@ -119,6 +170,12 @@ TClingMethodInfo& TClingMethodInfo::operator=(const TClingMethodInfo &rhs) {
       fTemplateSpecIter = new SpecIterator(*rhs.fTemplateSpecIter);
    }
 
+   if (rhs.fUsingIter) {
+      // The UsingIterator query the decl.
+      R__LOCKGUARD(gInterpreterMutex);
+      fUsingIter = new UsingIterator(*rhs.fUsingIter);
+   }
+
    return *this;
 }
 
@@ -126,7 +183,7 @@ TClingMethodInfo& TClingMethodInfo::operator=(const TClingMethodInfo &rhs) {
 TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
                                    TClingClassInfo *ci)
    : TClingDeclInfo(nullptr), fInterp(interp), fFirstTime(true), fContextIdx(0U), fTitle(""),
-     fTemplateSpecIter(0)
+     fTemplateSpecIter(0), fUsingIter(0)
 {
    R__LOCKGUARD(gInterpreterMutex);
 
@@ -155,7 +212,7 @@ TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
 TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
                                    const clang::FunctionDecl *FD)
    : TClingDeclInfo(FD), fInterp(interp), fFirstTime(true), fContextIdx(0U), fTitle(""),
-     fTemplateSpecIter(0)
+     fTemplateSpecIter(0), fUsingIter(0)
 {
 
 }
@@ -163,6 +220,7 @@ TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
 TClingMethodInfo::~TClingMethodInfo()
 {
    delete fTemplateSpecIter;
+   delete fUsingIter;
 }
 
 TDictionary::DeclId_t TClingMethodInfo::GetDeclId() const
@@ -216,6 +274,8 @@ void TClingMethodInfo::Init(const clang::FunctionDecl *decl)
    fIter = clang::DeclContext::decl_iterator();
    delete fTemplateSpecIter;
    fTemplateSpecIter = 0;
+   delete fUsingIter;
+   fUsingIter = 0;
    fDecl = decl;
 }
 
@@ -237,6 +297,10 @@ const clang::Decl* TClingMethodInfo::GetDeclSlow() const
       R__LOCKGUARD(gInterpreterMutex);
       cling::Interpreter::PushTransactionRAII RAII(fInterp);
       return *(*fTemplateSpecIter);
+   }  else if (fUsingIter) {
+      R__LOCKGUARD(gInterpreterMutex);
+      cling::Interpreter::PushTransactionRAII RAII(fInterp);
+      return *(*fUsingIter);
    }
    return *fIter;
 }
@@ -281,6 +345,9 @@ static bool HasUnexpandedParameterPack(clang::QualType QT, clang::Sema& S) {
 static void InstantiateFuncTemplateWithDefaults(clang::FunctionTemplateDecl* FTDecl,
                                                 clang::Sema& S,
                                                 const cling::LookupHelper& LH) {
+   // NO!!
+   return;
+
    // Force instantiation if it doesn't exist yet, by looking it up.
    using namespace clang;
 
@@ -412,6 +479,13 @@ int TClingMethodInfo::InternalNext()
             } else {
                return 1;
             }
+         } else if (fUsingIter) {
+            while (++(*fUsingIter)) {
+               if (*(*fUsingIter))
+                  return 1;
+            }
+            delete fUsingIter; fUsingIter = 0;
+            ++fIter;
          } else {
             ++fIter;
          }
@@ -459,6 +533,18 @@ int TClingMethodInfo::InternalNext()
          }
       }
 
+
+      clang::UsingDecl* udecl =
+          llvm::dyn_cast<clang::UsingDecl>(*fIter);
+
+      if ( udecl ) {
+          // A UsingDecl potentially brings in a bunch of functions, so
+          // start an inner loop to catch them all
+          delete fUsingIter;
+          fUsingIter = new UsingIterator(fInterp, udecl);
+          return 1;
+      }
+
       // Return if this decl is a function or method.
       if (llvm::isa<clang::FunctionDecl>(*fIter)) {
          // Iterator is now valid.
diff --git a/src/core/metacling/src/TClingMethodInfo.h b/src/core/metacling/src/TClingMethodInfo.h
index 573581d09c..5baf8ed93b 100644
--- a/src/core/metacling/src/TClingMethodInfo.h
+++ b/src/core/metacling/src/TClingMethodInfo.h
@@ -54,6 +54,7 @@ class TClingTypeInfo;
 class TClingMethodInfo final : public TClingDeclInfo {
 private:
    class SpecIterator;
+   class UsingIterator;
 
    cling::Interpreter                          *fInterp; // Cling interpreter, we do *not* own.
    llvm::SmallVector<clang::DeclContext *, 2>   fContexts; // Set of DeclContext that we will iterate over.
@@ -62,13 +63,14 @@ private:
    clang::DeclContext::decl_iterator            fIter; // Our iterator.
    std::string                                  fTitle; // The meta info for the method.
    SpecIterator                                *fTemplateSpecIter; // Iter over template specialization. [We own]
+   UsingIterator                               *fUsingIter; // for internal loop over using functions. [We own]
 
    const clang::Decl* GetDeclSlow() const;
 
 public:
    explicit TClingMethodInfo(cling::Interpreter *interp)
       : TClingDeclInfo(nullptr), fInterp(interp), fFirstTime(true), fContextIdx(0U), fTitle(""),
-        fTemplateSpecIter(0) {}
+        fTemplateSpecIter(0), fUsingIter(0) {}
 
    TClingMethodInfo(const TClingMethodInfo&);
    TClingMethodInfo& operator=(const TClingMethodInfo &in);
diff --git a/src/core/meta/inc/TListOfFunctions.h b/src/core/meta/inc/TListOfFunctions.h
index 58fae3025d..5e8b74c936 100644
--- a/src/core/meta/inc/TListOfFunctions.h
+++ b/src/core/meta/inc/TListOfFunctions.h
@@ -79,7 +79,7 @@ public:
 
 
    TFunction *Find(DeclId_t id) const;
-   TFunction *Get(DeclId_t id);
+   TFunction *Get(DeclId_t id, bool verify = true);
 
    void       AddFirst(TObject *obj);
    void       AddFirst(TObject *obj, Option_t *opt);
diff --git a/src/core/meta/src/TListOfFunctions.cxx b/src/core/meta/src/TListOfFunctions.cxx
index 5248cb8d0d..916e9a1e8e 100644
--- a/src/core/meta/src/TListOfFunctions.cxx
+++ b/src/core/meta/src/TListOfFunctions.cxx
@@ -259,7 +259,7 @@ TFunction *TListOfFunctions::Find(DeclId_t id) const
 /// Return (after creating it if necessary) the TMethod or TFunction
 /// describing the function corresponding to the Decl 'id'.
 
-TFunction *TListOfFunctions::Get(DeclId_t id)
+TFunction *TListOfFunctions::Get(DeclId_t id, bool verify)
 {
    if (!id) return 0;
 
@@ -268,10 +268,12 @@ TFunction *TListOfFunctions::Get(DeclId_t id)
    TFunction *f = Find(id);
    if (f) return f;
 
-   if (fClass) {
-      if (!gInterpreter->ClassInfo_Contains(fClass->GetClassInfo(),id)) return 0;
-   } else {
-      if (!gInterpreter->ClassInfo_Contains(0,id)) return 0;
+   if (verify) {
+      if (fClass) {
+         if (!gInterpreter->ClassInfo_Contains(fClass->GetClassInfo(),id)) return 0;
+      } else {
+         if (!gInterpreter->ClassInfo_Contains(0,id)) return 0;
+      }
    }
 
    MethodInfo_t *m = gInterpreter->MethodInfo_Factory(id);
@@ -393,7 +395,7 @@ void TListOfFunctions::Load()
          TDictionary::DeclId_t mid = gInterpreter->GetDeclId(t);
          // Get will check if there is already there or create a new one
          // (or re-use a previously unloaded version).
-         Get(mid);
+         Get(mid, false /* verify */);
       }
    }
    gInterpreter->MethodInfo_Delete(t);
diff --git a/src/core/meta/inc/TListOfFunctionTemplates.h b/src/core/meta/inc/TListOfFunctionTemplates.h
index 1534cdc680..cc90b1eb3a 100644
--- a/src/core/meta/inc/TListOfFunctionTemplates.h
+++ b/src/core/meta/inc/TListOfFunctionTemplates.h
@@ -62,7 +62,7 @@ public:
    virtual TList     *GetListForObject(const char* name) const;
    virtual TList     *GetListForObject(const TObject* obj) const;
 
-   TFunctionTemplate *Get(DeclId_t id);
+   TFunctionTemplate *Get(DeclId_t id, bool verify = true);
 
    void       AddFirst(TObject *obj);
    void       AddFirst(TObject *obj, Option_t *opt);
diff --git a/src/core/meta/src/TListOfFunctionTemplates.cxx b/src/core/meta/src/TListOfFunctionTemplates.cxx
index 2602b33b0d..00a7e49853 100644
--- a/src/core/meta/src/TListOfFunctionTemplates.cxx
+++ b/src/core/meta/src/TListOfFunctionTemplates.cxx
@@ -250,16 +250,18 @@ TList* TListOfFunctionTemplates::GetListForObject(const TObject* obj) const
 /// Return (after creating it if necessary) the TMethod or TFunction
 /// describing the function corresponding to the Decl 'id'.
 
-TFunctionTemplate *TListOfFunctionTemplates::Get(DeclId_t id)
+TFunctionTemplate *TListOfFunctionTemplates::Get(DeclId_t id, bool verify)
 {
    if (!id) return 0;
 
    TFunctionTemplate *f = (TFunctionTemplate*)fIds->GetValue((Long64_t)id);
    if (!f) {
-      if (fClass) {
-         if (!gInterpreter->ClassInfo_Contains(fClass->GetClassInfo(),id)) return 0;
-      } else {
-         if (!gInterpreter->ClassInfo_Contains(0,id)) return 0;
+      if (verify) {
+         if (fClass) {
+            if (!gInterpreter->ClassInfo_Contains(fClass->GetClassInfo(),id)) return 0;
+         } else {
+            if (!gInterpreter->ClassInfo_Contains(0,id)) return 0;
+         }
       }
 
       R__LOCKGUARD(gInterpreterMutex);
diff --git a/src/core/metacling/src/TCling.cxx b/src/core/metacling/src/TCling.cxx
index 9a59030df4..a89ea97a87 100644
--- a/src/core/metacling/src/TCling.cxx
+++ b/src/core/metacling/src/TCling.cxx
@@ -3943,11 +3943,17 @@ void TCling::LoadFunctionTemplates(TClass* cl) const
            declEnd = allDeclContexts.end(); declIter != declEnd; ++declIter) {
          // Iterate on all decls for each context.
          for (clang::DeclContext::decl_iterator DI = (*declIter)->decls_begin(),
               DE = (*declIter)->decls_end(); DI != DE; ++DI) {
             if (const clang::FunctionTemplateDecl* FTD = dyn_cast<clang::FunctionTemplateDecl>(*DI)) {
-                  funcTempList->Get(FTD);
+               funcTempList->Get(FTD);
+            } else if (const clang::UsingDecl* UD = llvm::dyn_cast<clang::UsingDecl>(*DI)) {
+               for (auto it = UD->shadow_begin(); it != UD->shadow_end(); ++it) {
+                  if ((FTD = dyn_cast<clang::FunctionTemplateDecl>(it->getTargetDecl()))) {
+                     funcTempList->Get(FTD, false);
+                  }
+               }
             }
          }
       }
    }
 }
